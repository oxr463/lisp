#|
  Exploring Common Sets 
  By Lucas Ramage
|#

; Sets are collections of objects.
(list 1 2 3 4)

; Elements are objects in a set.
(let e)

; A null set is an empty set.
(list)
