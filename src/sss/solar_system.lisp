#|
	Solar System Simulation
	By Lucas Ramage
|#

;; Load our planets
(load "planets.lisp")

;; These control the shape of the orbits
(defparameter *baseSize* 50)
(defparameter *incrX* 30)
(defparameter *incrY* 20)

;; Based on Processing.js ellipse()
;; http://processingjs.org/reference/ellipse_/
(defun ellipse(x y width height)
  x y width height)

(defun drawPlanet(orbit size speed time)
  (let ((degree (mod (* time speed)) 360)
		(radiusX (/ (+ *baseSize* (* *incrX* (expt orbit 1.1))) 2))
		(radiusY (/ (+ *baseSize* (* *incrY* (expt orbit 1.1))) 2))
		(positionX (* radiusX (cos degree)))
		(positionY (* radiusY (sin degree))))
	
	(ellipse (positionX positionY size size))
	
	(if (eql orbit (planet-orbit *saturn*))
	  (ellipse positionX positionY (* 1.5 size) (* .25 size)))))
