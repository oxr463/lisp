#|
	Solar System Simulation
	By Lucas Ramage
|#

;;; Planets
(defstruct planet moons name orbit size speed)

;; Earth
(defparameter *earth*
	(make-planet :name "earth"
				 :moons 1
				 :orbit 3
				 :size 10
				 :speed 1))

;; Jupiter
(defparameter *jupiter*
	(make-planet :name "jupiter"
				 :moons 67
				 :orbit 5
				 :size 20
				 :speed (/ 1 11.862)))

;; Mars
(defparameter *mars*
	(make-planet :name "mars"
				 :moons 2
				 :orbit 4
				 :size 7
				 :speed (/ 365.26 686.98)))

;; Mercury
(defparameter *mercury*
	(make-planet :name "mercury"
				 :moons 0
				 :orbit 1
				 :size 5
				 :speed (/ 365.26 87.96)))

;; Neptune
(defparameter *neptune*
	(make-planet :name "neptune"
				 :moons 14
				 :orbit 8
				 :size 10
				 :speed (/ 1 164.81)))

;; Pluto
(defparameter *pluto*
	(make-planet :name "pluto"
				 :moons 5
				 :orbit 9
				 :size 5
				 :speed (/ 1 247.7)))

;; Saturn
(defparameter *saturn*
	(make-planet :name "saturn"
				 :moons 62
				 :orbit 6
				 :size 15
				 :speed (/ 1 29.456)))

;; Uranus
(defparameter *uranus*
	(make-planet :name "uranus"
				 :moons 27
				 :orbit 7
				 :size 10
				 :speed (/ 1 84.07)))

;; Venus
(defparameter *venus*
	(make-planet :name "venus"
				 :moons 0
				 :orbit 2
				 :size 10
				 :speed (/ 365.26 224.68)))

