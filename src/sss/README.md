# Solar System Simulation

## Reference

- [The Solar System](https://www.khanacademy.org/computer-programming/the-solar-system/957249502)

- [The Planets (plus the Dwarf Planet Pluto)](https://www.enchantedlearning.com/subjects/astronomy/planets)

- [Make Your Own Solar System](https://nrich.maths.org/content/id/7753/STEMclubs-SolarSystem-PlanetaryData-diy.pdf)

- [Make Your Own Solar System - Planetary Data](https://nrich.maths.org/content/id/7753/STEMclubs-SolarSystem-PlanetaryData-basic.pdf)
