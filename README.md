# lisp

_The beginnings of my love affair with the lisps._

## License

SPDX-License-Identifier: [0BSD](https://spdx.org/licenses/0BSD.html)

## Reference

- [Basic Lisp Programming](https://www.cs.sfu.ca/CourseCentral/310/pwfong/Lisp/1/tutorial1.html)

- [Common Lisp Wiki](http://www.cliki.net/)

- [Embeddable Common-Lisp](https://common-lisp.net/project/ecl/)

- [Google Common Lisp Style Guide](https://google.github.io/styleguide/lispguide.xml)

- [Hylang](http://docs.hylang.org/en/stable/index.html)

- [Indentation](http://dept-info.labri.fr/~strandh/Teaching/PFS/Common/Strandh-Tutorial/indentation.html)

- [Learn Lisp in Y](https://learnxinyminutes.com/docs/common-lisp/)

- [Lisp](http://www.math-cs.gordon.edu/courses/cs323/LISP/lisp.html)

- [Lisp Flavored Erlang - Gitbook](https://lfe.gitbooks.io/)

- [Practical Common Lisp](http://www.gigamonkeys.com/book/)
